import http from 'k6/http';
import {check} from 'k6';

export const options = {
    vus: 2,
    duration: '10s',

};

export default function () {

    const url = 'http://localhost:32767/api/predizer_categoria';
    const payload = JSON.stringify({
        descricao: 'Kit 4 Esponjas Maquiagem Formato de Coração',
    });
    const params = {
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = http.post(url, payload, params);

    check(response, {
        'is status 200': (r) => r.status === 200,
        'is duration < 200ms': (r) => r.timings.duration < 200,
        'classify as maquiagem': (r) => r.json().categoria === 'maquiagem',
    });


}